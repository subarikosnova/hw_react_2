import React, {useEffect, useState} from "react";
import Card from "./components/Card/Card";
import Modal from "./components/Modal/Modal";
import style from './App.css'
import Header from "./components/Header/Header";




const App = () => {

    const [data, setData] = useState(false)
    const [modal, setModal] = useState(false)
    const [selectedItem, setSelectedItem] = useState(null);
    const [cart, setCart] = useState([]);


    const addToCart = (item) => {
        const existingItemIndex = cart.findIndex((cartItem) => cartItem.id === item.id);

        if (existingItemIndex !== -1) {
            const updatedCart = [...cart];
            updatedCart[existingItemIndex].amount += 1;
            setCart(updatedCart);
        } else {
            setCart([...cart, { ...item, amount: 1 }]);
        }

        setModal(false);
    };

    const removeFromCart = (itemId) => {
        const updatedCart = cart.filter((item) => item.id !== itemId);
        setCart(updatedCart);
    };




    useEffect(() => {
        const storedCart = JSON.parse(localStorage.getItem("cart")) || [];
        setCart(storedCart);
    }, []);

    useEffect(() => {
        localStorage.setItem("cart", JSON.stringify(cart));
    }, [cart]);

    useEffect(() => {
        const fetchItems = async () => {
            const response = await fetch(`/product.json`);
            const fetchedData = await response.json();
            setData(fetchedData);

            console.log(fetchedData);
        };
        fetchItems();
    }, []);


    function modalOpen() {
        setModal(true)
    }

    function modalClose() {
        setModal(false)
    }

    return (

        <div>
            {modal && <Modal onCancel={modalClose} onBuy={() => addToCart(selectedItem)} isOpen={modal} />}

            <Header cart={cart} removeFromCart={removeFromCart}/>
        <main>
            {data.length ? (
                data.map((item , index) => (
                    <Card
                        key={index}
                        id={item.id}
                        active={() => {
                            setSelectedItem(item);
                            modalOpen();
                        }}
                        name={item.name}
                        price={item.price}
                        imageSrc={item.image}
                        article={item.article}
                        color={item.color}
                    />
                ))
            ) : (
                <p>Загрузка данных...</p>
            )}
        </main>
        </div>
    );
};

export default App;