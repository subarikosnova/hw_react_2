import {useState , useEffect} from "react";
import PropTypes from 'prop-types'
import { FaStar } from "react-icons/fa6";

function Card({active , id , name , price , imageSrc , article ,color }) {
    const [starActive, setStarActive] = useState(false);

    useEffect(() => {
        const storedStarActive = localStorage.getItem(`starActive_${id}`);
        if (storedStarActive !== null) {
            setStarActive(JSON.parse(storedStarActive));
        }
    }, [id]);

    const handleStarClick = () => {
        setStarActive((prevStarActive) => {
            const newStarActive = !prevStarActive;
            localStorage.setItem(`starActive_${id}`, JSON.stringify(newStarActive));
            return newStarActive;
        });
    };

    return (
        <div  id={id} className="card" >
            <FaStar onClick={handleStarClick} className={`star ${starActive ? 'star-active' : ''}`} />
            <img src={imageSrc} alt="#"/>
            <h2>Name: {name}</h2>
            <p>Article: {article}</p>
            <p>Price: {price}$</p>
            <p>Color: {color}</p>
            <button onClick={active}>Add to cart</button>
        </div>
)}

Card.protoTypes= {
    price: PropTypes.number,
    name: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    imgSrc: PropTypes.any
}
export default Card;

