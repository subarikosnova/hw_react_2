import { FaCartShopping } from "react-icons/fa6";
import { FaTrashAlt } from "react-icons/fa";
import React ,{useState} from "react";
import PropTypes from "prop-types";




function Header({cart , removeFromCart }) {
    let [cartOpen , setCartOpen] =useState(false)
    return (
        <div className='wrapper'>
            <h2>DOTA ALL STARS SHOP</h2>
            <button>Home</button>
            <button>Contact</button>
            <button>About Us</button>
            <FaCartShopping  onClick={() => setCartOpen(!cartOpen)}
                             className={`shopCartBtn ${cartOpen && 'shopCartBtn-active'}`} />

            { cartOpen &&
                <div className="shop-cart">
                    <h3>Shopping Cart</h3>
                    {cart.length > 0 ? (
                        <div className="shop-cart_content">
                            {cart.map((item) => (
                                <div className="shop-cart_content-wrapper" key={item.id}>
                                    <div className="img-wrapper">
                                    <img src={item.image} alt="#"/>
                                    </div>
                                    <div className="text-wrapper">
                                    <p>Name: {item.name} </p>
                                    <p>Price: {item.price}</p>
                                    <p>Article: {item.article}</p>
                                    <p>Color: {item.color}</p>
                                    <p>Amount : {item.amount}</p>
                                    </div>
                                    <FaTrashAlt className="btn-trash" onClick={() => removeFromCart(item.id)}/>


                                </div>
                            ))}
                        </div>
                    ) : (
                        <p className="empty">No Item</p>
                    )}
                </div>
            }
        </div>
    )
}

Header.protoTypes= {
    price: PropTypes.number,
    name: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    amount: PropTypes.number
}

export default Header