import React from 'react';
import modalStyle from './Modal.module.css'
function Modal({ onCancel , onBuy}) {
    const handleOverlayClick = (event) => {
        if (event.target.classList.contains('overlay')) {
            onCancel();
        }
    };

    return (
        <div className="overlay" onClick={handleOverlayClick}>
            <div className="modal-content" >
             <div className="modal-content_title">
            <h2>Want to buy ?</h2>
            <span onClick={onCancel}>&times;</span>
             </div>
            <p>If you sure to confirm click Buy </p>
            <div className="modal-content_btn-group">
            <button onClick={onBuy}>Buy</button>
            <button onClick={onCancel}>Close</button>
            </div>
            </div>
        </div>

    );
}

export default Modal;